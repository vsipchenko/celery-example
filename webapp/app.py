import os

from celery.result import AsyncResult
from flask import request
from flask import Flask
from celery import Celery

CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND", 'redis://localhost:6379/0')
CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", 'redis://localhost:6379/1')
celeryapp = Celery('tasks',  backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)
celeryapp.config_from_object('celeryconfig')


app = Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    return 'Hello'


@app.route('/trigger_task', methods=['POST'])
def trigger_task():
    r = celeryapp.send_task('tasks.do_smth', args=(100, ))
    return r.id


@app.route('/trigger_task_res', methods=['GET'])
def trigger_task_res():
    task_id = request.args.get('task_id')
    result = AsyncResult(task_id, app=celeryapp)
    if result.ready():
        return result.get()
    return result.state


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='5000')
