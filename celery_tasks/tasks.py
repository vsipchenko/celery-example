from time import sleep
import os

from celery import Celery
CELERY_RESULT_BACKEND = os.environ.get("CELERY_RESULT_BACKEND", 'redis://localhost:6379/0')
CELERY_BROKER_URL = os.environ.get("CELERY_BROKER_URL", 'redis://localhost:6379/1')
celeryapp = Celery('tasks',  backend=CELERY_RESULT_BACKEND, broker=CELERY_BROKER_URL)


@celeryapp.task
def do_smth(a):
    print('!' * 100)
    print(a)
    print('!' * 100)
    sleep(int(a))
    return 'OK'
